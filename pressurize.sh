#!/bin/bash

##
# @file pressurize.sh
# @brief Installs pressure for Steam on Windows
# @author August Valera
# @version
# @date 2017-07-05
#

PressureRepo="DirtDiglett/Pressure2"
echo -n "Repo url ($PressureRepo): "
read -t 5 Input
[[ ! -z $Input ]] && PressureRepo=$Input

PressureZipUrl=$(curl -s https://api.github.com/repos/$PressureRepo/releases/latest | grep browser_download_url | cut -d '"' -f 4)
echo "Latest release for $PressureRepo: $PressureZipUrl"

TempDir=$(mktemp -d)
echo "Creating temporary directory: $TempDir"

cd $TempDir
! type wget && sudo apt-get install -y wget
wget $PressureZipUrl

ls

PressureZip=$(basename $PressureZipUrl)
! type unzip && sudo apt-get install -y unzip
unzip $PressureZip
ls

PressureFolder=$(basename $PressureRepo)
SteamFolder="/mnt/c/Program Files (x86)/Steam"
SkinsFolder="$SteamFolder/skins"
if [[ -e "$SteamFolder" ]]; then
  mkdir -vp "$SkinsFolder/$PressureFolder"
  cp -rv $PressureFolder/* "$SkinsFolder/$PressureFolder"
else
  echo "KO: Steam is not installed in the usual place, cannot pressurize: $SteamFolder"
fi

rm -rf $TempDir
echo "OK"
