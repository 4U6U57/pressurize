# Pressurize

Pressurize is a bash script for [Windows Subsystem for Linux](http://aka.ms/wsl)
that installs the [Pressure](http://pressureforsteam.com) Material Design theme
for Steam on Windows.

Yes, it is a very niche script, but it serves me well.
